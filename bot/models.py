from django.db import models

# Create your models here.



class Menu(models.Model):
    menu = models.CharField(max_length=20000)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
       return str(self.created_at)

class Log(models.Model):
    type = models.CharField(max_length=10)
    user_key = models.CharField(max_length=256)
    content = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
       return '{} {} {}'.format(self.type, self.content, self.user_key[:10])

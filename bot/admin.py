from django.contrib import admin

from .models import Menu, Log

admin.site.register(Menu)
admin.site.register(Log)
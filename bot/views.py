
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import (
                         HttpResponse,
                         JsonResponse,
                         )
from django.utils.timezone import datetime
from .crawl import crawl_menu

from .models import Menu, Log
import json


def get_menu(place, part=0,date=''):
    menu = Menu.objects.filter(created_at__date=datetime.today())
    if menu:
        menu = json.loads(menu[0].menu)
    else:
        menu = crawl_menu(date)
        Menu.objects.create(menu=json.dumps(menu))
    menu = menu[place]
    if place in ['dorm', 'prof']:
        menu = menu[part]

    if not menu:
        menu = '등록된 식단이 없습니다.'
    else:
        menu = '\n'.join(menu)
        menu = menu.replace('<','\n<').strip()
    return menu


buttons = ["학생 식당", "기숙사 식당 - 아침", "기숙사 식당 - 점심", "기숙사 식당 - 저녁", "교직원 식당(생활관 2층) - 점심", "교직원 식당(생활관 2층) - 저녁"]

def keyboard(request):
    return JsonResponse({
      "type" : "buttons",
      "buttons" : buttons,
    })

@csrf_exempt
def message(request):
    data = json.loads(request.body.decode('utf-8'))
    content = data['content']
    if content == '학생 식당':
        menu = get_menu('student')
    elif content == '기숙사 식당 - 아침':
        menu = get_menu('dorm', 0)
    elif content == '기숙사 식당 - 점심':
        menu = get_menu('dorm', 1)
    elif content == '기숙사 식당 - 저녁':
        menu = get_menu('dorm', 2)
    elif content == '교직원 식당(생활관 2층) - 점심':
        menu = get_menu('prof', 1)
    elif content == '교직원 식당(생활관 2층) - 저녁':
        menu = get_menu('prof', 2)
    else:
        menu = '알 수 없는 명령어입니다.'
    text = menu
    Log.objects.create(type='message', content=content, user_key=data['user_key'])
    return JsonResponse({
      "message": {
        "text": text
      },
      'keyboard': {
            'type': 'buttons',
            'buttons': buttons
        }
    })


@csrf_exempt
def friend(request, user_key):
    if not user_key: #add
        data = json.loads(request.body.decode('utf-8'))
        user_key = data['user_key']
        Log.objects.create(type='friend_add', user_key=user_key)
    else:
        Log.objects.create(type='friend_del', user_key=user_key)
    return HttpResponse('')


@csrf_exempt
def chat_room(request, user_key):
    Log.objects.create(type='chat_exit', user_key=user_key)
    return HttpResponse('')

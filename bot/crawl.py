from bs4 import BeautifulSoup
import requests 
import re
import json

def crawl_menu(date=''):
	if date:
		url = 'http://www.ajou.ac.kr/main/life/food.jsp?date={}'.format(date)
	else:
		url = 'http://www.ajou.ac.kr/main/life/food.jsp'
	res = requests.get(url)

	html = res.content
	bs = BeautifulSoup(html,'html.parser')

	tables = bs.select('.ajou_table')

	try:
		student_menu = list(tables[0].select('ul')[0].stripped_strings)
	except:
		student_menu = []


	dorm_menu = [None, None, None]
	for i,part in enumerate(['아침','점심','저녁']):
		try:
			dorm_menu[i] = list(tables[1].find('td',text=re.compile(part)).findNext('ul').stripped_strings)
		except:
			dorm_menu[i] = []
			continue

	prof_menu = [None, None, None]
	for i,part in enumerate(['아침','점심','저녁']):
		try:
			prof_menu[i] = list(tables[2].find('td',text=re.compile(part)).findNext('ul').stripped_strings)
		except:
			prof_menu[i] = []
			continue


	menu = {'student': student_menu,
			 'dorm':dorm_menu,
			'prof':prof_menu}
	return menu
